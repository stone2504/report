package com.eburtis.report.controleur.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.eburtis.report.modele.constantes.StatutEnvoi;

public class CraDto extends AbstractDto {

	private Long id;
	private LocalDate date;
	private String contenu;
	private StatutEnvoi statutEnvoi;
	private LocalDateTime dateEnvoi;
	private UtilisateurDto utilisateur;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public StatutEnvoi getStatutEnvoi() {
		return statutEnvoi;
	}

	public void setStatutEnvoi(StatutEnvoi statutEnvoi) {
		this.statutEnvoi = statutEnvoi;
	}

	public LocalDateTime getDateEnvoi() {
		return dateEnvoi;
	}

	public void setDateEnvoi(LocalDateTime dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}

	public UtilisateurDto getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurDto utilisateur) {
		this.utilisateur = utilisateur;
	}
}
