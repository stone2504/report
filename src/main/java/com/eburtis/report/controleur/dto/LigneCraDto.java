package com.eburtis.report.controleur.dto;

public class LigneCraDto extends AbstractDto {

	private Long id;
	private String commentaire;
	private CraDto cra;
	private SousTacheDto sousTache;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public CraDto getCra() {
		return cra;
	}

	public void setCra(CraDto cra) {
		this.cra = cra;
	}

	public SousTacheDto getSousTache() {
		return sousTache;
	}

	public void setSousTache(SousTacheDto sousTache) {
		this.sousTache = sousTache;
	}
}
