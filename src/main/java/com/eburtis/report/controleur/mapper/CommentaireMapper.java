package com.eburtis.report.controleur.mapper;

import org.mapstruct.Mapper;

import com.eburtis.report.controleur.dto.CommentaireDto;
import com.eburtis.report.controleur.mapper.abstraction.IMapper;
import com.eburtis.report.modele.entite.Commentaire;

/**
 * Mapper CommentaireDto/Commentaire
 */
@Mapper
public interface CommentaireMapper extends IMapper<CommentaireDto, Commentaire> {

}
