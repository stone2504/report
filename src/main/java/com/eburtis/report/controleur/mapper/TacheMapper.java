package com.eburtis.report.controleur.mapper;

import org.mapstruct.Mapper;

import com.eburtis.report.controleur.dto.TacheDto;
import com.eburtis.report.controleur.mapper.abstraction.IMapper;
import com.eburtis.report.modele.entite.Tache;

/**
 * Mapper TacheDto/Tache
 */
@Mapper
public interface TacheMapper extends IMapper<TacheDto, Tache> {

}
