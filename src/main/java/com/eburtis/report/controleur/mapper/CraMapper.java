package com.eburtis.report.controleur.mapper;

import org.mapstruct.Mapper;

import com.eburtis.report.controleur.dto.CraDto;
import com.eburtis.report.controleur.mapper.abstraction.IMapper;
import com.eburtis.report.modele.entite.Cra;

/**
 * Mapper CraDto/Cra
 */
@Mapper
public interface CraMapper extends IMapper<CraDto, Cra> {

}
