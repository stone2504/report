package com.eburtis.report.controleur.mapper;

import org.mapstruct.Mapper;

import com.eburtis.report.controleur.dto.EquipeDto;
import com.eburtis.report.controleur.mapper.abstraction.IMapper;
import com.eburtis.report.modele.entite.Equipe;

/**
 * Mapper EquipeDto/Equipe
 */
@Mapper
public interface EquipeMapper extends IMapper<EquipeDto, Equipe> {

}
