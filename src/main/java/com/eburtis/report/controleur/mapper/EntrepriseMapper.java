package com.eburtis.report.controleur.mapper;

import org.mapstruct.Mapper;

import com.eburtis.report.controleur.dto.EntrepriseDto;
import com.eburtis.report.controleur.mapper.abstraction.IMapper;
import com.eburtis.report.modele.entite.Entreprise;

/**
 * Mapper EntrepriseDto/Entreprise
 */
@Mapper
public interface EntrepriseMapper extends IMapper<EntrepriseDto, Entreprise> {

}
