package com.eburtis.report.controleur.implementation;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eburtis.report.controleur.CrudControleur;
import com.eburtis.report.controleur.dto.EquipeDto;
import com.eburtis.report.controleur.mapper.EquipeMapper;
import com.eburtis.report.service.implementation.EquipeService;

@RestController
@RequestMapping(value = {"/ws/equipe"})
public class EquipeControleur implements CrudControleur<EquipeDto> {

	private final EquipeMapper equipeMapper;
	private final EquipeService equipeService;

	public EquipeControleur(EquipeMapper equipeMapper, EquipeService equipeService) {
		this.equipeMapper = equipeMapper;
		this.equipeService = equipeService;
	}

	@Override
	public void creerOuModifier(EquipeDto dto) {

	}

	@Override
	public void creerOuModifierTous(Collection<EquipeDto> dtos) {

	}

	@Override
	@DeleteMapping
	public void supprimer(@RequestParam(value = "id") long id) {

	}

	@Override
	public void supprimerTous(Collection<EquipeDto> dtos) {

	}

	@Override
	@GetMapping
	public List<EquipeDto> lister() {
		return equipeMapper.mapModelsToDtos(equipeService.lister());
	}

	@Override
	public Set<EquipeDto> listerSansDoublon() {
		return null;
	}

	@Override
	public Map<Long, EquipeDto> listerParId() {
		return null;
	}
}
