package com.eburtis.report.modele.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eburtis.report.modele.entite.Cra;

public interface CraRepository extends JpaRepository<Cra, Long>, CrudRepository {

}
