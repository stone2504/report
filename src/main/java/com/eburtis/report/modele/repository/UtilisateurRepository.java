package com.eburtis.report.modele.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eburtis.report.modele.entite.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long>, CrudRepository {

	List<Utilisateur> findByCodeLike(String code);
}
