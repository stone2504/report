package com.eburtis.report.service.implementation;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.eburtis.report.modele.entite.Equipe;
import com.eburtis.report.modele.repository.EquipeRepository;
import com.eburtis.report.service.CrudService;

@Service
public class EquipeService implements CrudService<Equipe> {

	private final EquipeRepository equipeRepository;

	public EquipeService(EquipeRepository equipeRepository) {
		this.equipeRepository = equipeRepository;
	}

	@Override
	public void creerOuModifier(Equipe entity) {

	}

	@Override
	public void creerOuModifierTous(Collection<Equipe> entities) {

	}

	@Override
	public void supprimer(long id) {

	}

	@Override
	public void supprimerTous(Collection<Equipe> entities) {

	}

	@Override
	public List<Equipe> lister() {
		return equipeRepository.findAll();
	}

	@Override
	public Set<Equipe> listerSansDoublon() {
		return null;
	}

	@Override
	public Map<Long, Equipe> listerParId() {
		return null;
	}
}
