INSERT INTO public.entreprise
(id, adresse, code, contact, email, pays, raison_sociale, "type", ville)
VALUES
(1, 'Cocody Riviera 3', 'EBU', '+2252722459610', 'info@eburtis.ci', 'Côte d''Ivoire', 'Eburtis', 'SARL', 'Abidjan'),
(2, '375 rue Juliette Récamier – 69970 Chaponnay ', 'GFIT', '+330102030405', 'info@gfit.fr', 'France', 'GFIT', 'SARL', 'Lyon');
