-- public.sous_tache definition

-- Drop table

-- DROP TABLE public.sous_tache;

CREATE TABLE public.sous_tache (
	id int8 NOT NULL,
	code varchar(255) NULL,
	description varchar(255) NULL,
	designation varchar(255) NULL,
	ligne_cra_id int8 NOT NULL,
	tache_id int8 NOT NULL,
	CONSTRAINT sous_tache_pkey PRIMARY KEY (id),
	CONSTRAINT uk_1v4moivpet5if7rg51mjk10wf UNIQUE (ligne_cra_id)
);


-- public.sous_tache foreign keys

ALTER TABLE public.sous_tache ADD CONSTRAINT fk8tlytvx8jpwbvfh0pktsfsfdt FOREIGN KEY (tache_id) REFERENCES public.tache(id);
ALTER TABLE public.sous_tache ADD CONSTRAINT fkr1j51h9uomla9nfnykcobgjq4 FOREIGN KEY (ligne_cra_id) REFERENCES public.ligne_cra(id);
