-- public.commentaire definition

-- Drop table

-- DROP TABLE public.commentaire;

CREATE TABLE public.commentaire (
	id int8 NOT NULL,
	contenu varchar(255) NULL,
	date_heure timestamp NULL,
	cra_id int8 NOT NULL,
	utilisateur_id int8 NOT NULL,
	CONSTRAINT commentaire_pkey PRIMARY KEY (id)
);


-- public.commentaire foreign keys

ALTER TABLE public.commentaire ADD CONSTRAINT fkfkx1pegfdsd6e3cp2wblsc5jf FOREIGN KEY (utilisateur_id) REFERENCES public.utilisateur(id);
ALTER TABLE public.commentaire ADD CONSTRAINT fkrnaek505y1lrdpy6gx8bdc5bw FOREIGN KEY (cra_id) REFERENCES public.cra(id);
