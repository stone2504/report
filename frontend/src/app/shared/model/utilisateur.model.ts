import { Entreprise } from './entreprise.model';
import { Equipe } from './equipe.model';

export class Utilisateur {
	id?: number;
	code?: string;
	nom?: string;
	prenoms?: string;
	genre?: string;
	contact?: string;
	email?: string;
	role?: string;
	login?: string;
	password?: string;
	entreprise?: Entreprise;
	equipe?: Equipe;

	constructor(id?: number, code?: string, nom?: string, prenoms?: string, genre?: string, contact?: string, email?: string, role?: string,
				login?: string, password?: string, entreprise?: Entreprise, equipe?: Equipe) {
		this.id = id;
		this.code = code;
		this.nom = nom;
		this.prenoms = prenoms;
		this.genre = genre;
		this.contact = contact;
		this.email = email;
		this.role = role;
		this.login = login;
		this.password = password;
		this.entreprise = entreprise;
		this.equipe = equipe;
	}
}
