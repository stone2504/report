import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../../../shared/model/utilisateur.model';
import { UtilisateurService } from '../../../shared/service/utilisateur.service';
import { ConfirmationService } from 'primeng/api';

@Component({
	selector: 'app-utilisateur',
	templateUrl: './utilisateur.component.html',
	styleUrls: ['./utilisateur.component.scss']
})
export class UtilisateurComponent implements OnInit {

	utilisateurs: Utilisateur[] = [];
	utilisateurSelected!: Utilisateur | undefined;
	showModal = false;
	actionModal!: string;

	constructor(private utilisateurService: UtilisateurService, private confirmationService: ConfirmationService) {
	}

	ngOnInit(): void {
		this.listerUtilisateurs();
	}

	/**
	 * Ouvrir la popup de création d'utilisateur.
	 */
	ouvrirModalCreation() {
		this.showModal = true;
		this.actionModal = 'Créer';
		this.utilisateurSelected = new Utilisateur();
	}

	/**
	 * Ouvrir la popup de modification d'utilisateur.
	 *
	 * @param utilisateur l'utilisateur (Dans le cas d'un clic sur l'icône dans le tableau).
	 */
	ouvrirModalModification(utilisateur?: Utilisateur) {
		if (utilisateur) {
			this.onSeletedRow(utilisateur);
		}

		if(this.isUtilisateurSelected()) {
			this.showModal = true;
			this.actionModal = 'Modifier';
		}
	}

	/**
	 * Vide la sélection du tableau.
	 */
	onUnseletedRow(): void {
		this.utilisateurSelected = undefined;
	}

	/**
	 * Sélectionne un utilisateur du tableau.
	 */
	onSeletedRow(utilisateur: Utilisateur): void {
		this.utilisateurSelected = utilisateur;
	}

	/**
	 * Retourne true si un utilisateur est sélectionné.
	 */
	isUtilisateurSelected(): boolean {
		return this.utilisateurSelected !== undefined;
	}

	/**
	 * Supprime un utilisateur.
	 */
	supprimerUtilisateur(utilisateur: Utilisateur): void {
		this.utilisateurService.supprimer(utilisateur?.id!).subscribe(
				() => {
					this.showModal = false;
				}
		);
	}

	/**
	 * Supprime l'utilisation après confirmation.
	 * @param utilisateur
	 */
	confimerSuppression(utilisateur?: Utilisateur): void {
		if (utilisateur) {
			this.onSeletedRow(utilisateur);
		}

		this.confirmationService.confirm({
			message: 'Voulez-vous vraiment supprimer cet utilisateur ?',
			accept: () => {
				this.supprimerUtilisateur(utilisateur!);
			}
		});
	}

	/**
	 * Retourne true si l'utilisateur a un code.
	 *
	 * @param utilisateur l'utilisateur.
	 */
	hasCode(utilisateur: Utilisateur | undefined): boolean {
		return !!utilisateur?.code;
	}

	/**
	 * Génère le code de l'utilisateur.
	 */
	genererCodeUtilisateur(utilisateur: Utilisateur | undefined): void {
		utilisateur!.role = 'DEVELOPPEUR';
		this.utilisateurService.genererCode(utilisateur).subscribe(
				() => {
			this.listerUtilisateurs();
		});
	}

	/**
	 * Liste les utilisateurs
	 */
	listerUtilisateurs(): void {
		this.utilisateurService.lister().subscribe(
				(data) => {
					this.utilisateurs = data;
				}
		);
	}
}
