import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UtilisateurComponent } from './utilisateur.component';

const routes: Routes = [
	{
		path: '',
		component: UtilisateurComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})

/**
 *  Routing interne au module Contrôle.
 */
export class UtilisateurRoutingModule {
}
