import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { OrganisationComponent } from './organisation.component';

const routes: Routes = [
	{
		path: '',
		component: OrganisationComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})

/**
 *  Routing interne au module Contrôle.
 */
export class OrganisationRoutingModule {
}
